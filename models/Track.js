const mongoose = require("mongoose");

const TrackSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    duration: {
      type: String,
      required: true,
    },
    album: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Album",
      required: true,
    },
  },
  {
    versionKey: false,
  }
);

const Track = mongoose.model("Track", TrackSchema);
module.exports = Track;
