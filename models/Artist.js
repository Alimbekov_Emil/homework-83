const mongoose = require("mongoose");

const ArtistSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
      unique: true,
    },
    info: String,
    image: String,
  },
  {
    versionKey: false,
  }
);

const Artist = mongoose.model("Artist", ArtistSchema);
module.exports = Artist;
