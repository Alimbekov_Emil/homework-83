const express = require("express");
const router = express.Router();
const User = require("../models/User");
const Track = require("../models/Track");
const TrackHistory = require("../models/TrackHistory");

router.get("/track_history", async (req, res) => {
  try {
    const trackHistory = await TrackHistory.find();
    return res.send(trackHistory);
  } catch (e) {
    return res.status(401).send({ error: "Wrong Token!" });
  }
});

router.post("/track_history", async (req, res) => {
  try {
    const token = req.get("Authorization");

    if (!token) {
      return res.status(401).send({ error: "Unauthorized" });
    }

    const user = await User.findOne({ token });

    if (!user) {
      return res.status(401).send({ error: "Wrong Token!" });
    }
    const track = await Track.findOne({ _id: req.body.track });

    const datetime = new Date().toISOString();

    const trackHistory = new TrackHistory({ user: user._id, track: track._id, datetime });
    await trackHistory.save();

    return res.send(trackHistory);
  } catch (e) {
    return res.sendStatus(400);
  }
});

module.exports = router;
