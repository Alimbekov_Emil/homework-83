const express = require("express");
const router = express.Router();
const User = require("../models/User");

router.get("/", async (req, res) => {
  try {
    const user = await User.find();
    return res.send(user);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.post("/", async (req, res) => {
  try {
    const user = new User(req.body);
    user.generateToken();
    await user.save();

    return res.send(user);
  } catch (e) {
    return res.status(400).send(e);
  }
});

router.post("/sessions", async (req, res) => {
  try {
    const user = await User.findOne({ username: req.body.username });

    if (!user) {
      return res.status(401).send({ error: "Displayed an incorrect username or password" });
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
      return res.status(401).send({ error: "Displayed an incorrect username or password" });
    }

    user.generateToken();

    await user.save();

    return res.send({ token: user.token });
  } catch (e) {
    return res.status(400).send(e);
  }
});

module.exports = router;
